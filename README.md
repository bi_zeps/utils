[![Pipeline Master](https://img.shields.io/gitlab/pipeline/bi_zeps/utils/master?label=master&logo=gitlab)](https://gitlab.com/bi_zeps/utils)
[![License](https://img.shields.io/badge/dynamic/json?color=orange&label=license&query=%24.license.name&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F18136148%3Flicense%3Dtrue)](https://gitlab.com/bi_zeps/utils/-/blob/master/LICENSE)
[![Open Issues](https://img.shields.io/badge/dynamic/json?color=yellow&logo=gitlab&label=open%20issues&query=%24.statistics.counts.opened&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F18136148%2Fissues_statistics)](https://gitlab.com/bi_zeps/utils/-/issues)
[![Last Commit](https://img.shields.io/badge/dynamic/json?color=green&logo=gitlab&label=last%20commit&query=%24[:1].committed_date&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F18136148%2Frepository%2Fcommits%3Fbranch%3Dmaster)](https://gitlab.com/bi_zeps/utils/-/commits/master)

[![awsCli](https://badgen.net/badge/project/awsCli/orange?icon=gitlab)](https://gitlab.com/bi_zeps/utils/-/blob/master/README.md#awscli)
[![Stable Version](https://img.shields.io/docker/v/bizeps/awscli/stable?color=informational&label=stable&logo=docker)](https://gitlab.com/bi_zeps/utils/-/blob/master/CHANGELOG.md#awscli)
[![Docker Pulls](https://badgen.net/docker/pulls/bizeps/awscli?icon=docker&label=pulls)](https://hub.docker.com/r/bizeps/awscli)
[![Docker Image Size](https://badgen.net/docker/size/bizeps/awscli/stable?icon=docker&label=size)](https://hub.docker.com/r/bizeps/awscli)

[![certGenerator](https://badgen.net/badge/project/certGenerator/orange?icon=gitlab)](https://gitlab.com/bi_zeps/utils/-/blob/master/README.md#certgenerator)
[![Stable Version](https://img.shields.io/docker/v/bizeps/certgenerator/stable?color=informational&label=stable&logo=docker)](https://gitlab.com/bi_zeps/utils/-/blob/master/CHANGELOG.md#certgenerator)
[![Docker Pulls](https://badgen.net/docker/pulls/bizeps/certgenerator?icon=docker&label=pulls)](https://hub.docker.com/r/bizeps/certgenerator)
[![Docker Image Size](https://badgen.net/docker/size/bizeps/certgenerator/stable?icon=docker&label=size)](https://hub.docker.com/r/bizeps/certgenerator)

# awsCli
Amazon AWS command line interface to manage Amazon services.

`docker-compose run --rm awsCli help`

# certGenerator
A utility to create a self signed root CA and derived server and client certificate and key.
Be aware that in most cases the parameters for names and IPs have to be changed according to the network setup.

`docker-compose run --rm certGenerator [rebuild | all | clean | server | client]`

##  Usage
  - `docker-compose run --rm certGenerator`:
    * Removes current CA and certificates and generates new CA and certificates
    * Same as `docker-compose run --rm certGenerator rebuild`
  - `docker-compose run --rm certGenerator clean`
    * Removes the certificates in the `output`
  - `docker-compose run --rm certGenerator server`
    * Regenerates the server certificates
    * Does not generate CA if it already exist
  - `docker-compose run --rm certGenerator client`
    * Generates the client certificates
    * Does not generate CA if it already exist
  - `docker-compose up`
    * Short cut for generating the certificates
    * Does not remove the stopped container
    * `docker-compose rm` will be required

`docker-compose` helps to simplify the build, create and run process.
See `docker-compose --help` for further information.


##  Input
### Server Name and Server Alternative Name
By default, the server certificate is created with the following properties:
- CA_NAME=DockerHost
- SERVER_NAME=DockerHost
- SERVER_ALTNAMES=DNS:DockerHost,IP:127.0.0.1

The default properties can be overridden with the following command:
`docker-compose run --rm certGenerator "CA_NAME=MyHost" "SERVER_NAME=MyHost" "SERVER_ALTNAMES=DNS:MyHost,IP:127.0.0.1,IP:10.10.10.20"`

##  Output
  - Create/use `output` directory on host in the execution folder
  - Create or overwrite certificates in the output directory
    - output contains root CA
    - Server and client folders contain CA, certificate and the according key
